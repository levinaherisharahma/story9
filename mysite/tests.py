from django.test import LiveServerTestCase, TestCase, Client
from django.urls import resolve

# javascript ga usah dites karena merupakan client-side-server
class Test(TestCase):
	def test_ada_url_kosong(self):
		c = Client()
		response = c.get('//')
		self.assertEqual(response.status_code, 200)

	def test_halaman_utama_menggunakan_home_html(self):
		c = Client()
		response = c.get('//')
		self.assertTemplateUsed(response, 'home.html')
		self.assertTemplateUsed(response, 'base.html')

	def test_ada_tombol_login_logout(self):
		c = Client()
		response = c.get('//')
		content = response.content.decode('utf8')
		self.assertIn("<a", content)

	def test_ada_gambar(self):
		c = Client()
		response = c.get('//')
		content = response.content.decode('utf8')
		self.assertIn("<img", content)


from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

class FunctionalTest(LiveServerTestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(FunctionalTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(FunctionalTest, self).tearDown()

	def test_tombol_login_bisa_diklik(self):
		selenium = self.selenium
		selenium.get(self.live_server_url)

		login = selenium.find_element_by_id('login')
		login.send_keys(Keys.RETURN)

	def test_isi_form_login(self):
		selenium = self.selenium
		selenium.get('http://localhost:8000/accounts/login')
		user = selenium.find_element_by_id('id_username')
		password = selenium.find_element_by_id('id_password')
		submit = selenium.find_element_by_id('submit')

		user.send_keys('guest')
		password.send_keys('123')
		submit.send_keys(Keys.RETURN)

		def test_tombol_logout_bisa_diklik(self):
			selenium = self.selenium
			selenium.get(self.live_server_url)
			logout = selenium.find_element_by_id('logouot')
			logout.send_keys(Keys.RETURN)